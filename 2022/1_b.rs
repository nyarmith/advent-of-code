use std::io;

fn main(){
    let stdin = io::stdin();    // construct a handle to the std input of current process
    // each handle returned is a reference to a shared global buffer whose access is synchronized via a mutex

    let mut top_three = Vec::new();

    let mut tmp_buf : u64 = 0;
    for line in stdin.lines() {
        let my_str = line.expect("str err");
        match my_str.as_str() {
            "" => {
                top_three.push(tmp_buf);
                tmp_buf = 0;
            }
            _ => {
                tmp_buf += my_str.parse::<u64>().expect("Couldn't parse u64");
            }
        }
    }
    
    top_three.sort_by(|a, b| b.cmp(a));
    if top_three.len() > 3 { top_three.truncate(3); }
    
    let sum : u64 = top_three.iter().sum();
    println!("max cals: {}", sum);
}
