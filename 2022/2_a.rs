use std::io;
use std::str::Chars;
use GameMove::*;

#[derive(PartialEq, Eq, Copy, Clone)]
enum GameMove {
    Rock = 1,
    Paper = 2,
    Scissors = 3
}


trait FromChar {
    fn from_char(c: char) -> Self;
}

impl FromChar for GameMove {
    fn from_char(c: char) -> Self {
         match c {
            'A' => Rock,
            'B' => Paper,
            'C' => Scissors,
            'X' => Rock,
            'Y' => Paper,
            'Z' => Scissors,
             _  => panic!("Unexpected character {} in from_char", c)
         }
     }
}

fn score(other: GameMove, us: GameMove) -> u64 {
    let u = us as u64;
    match (other, us) {
        (Rock, Scissors) => u,  // lose cases
        (Paper, Rock) => u,
        (Scissors, Paper) => u,
        (Scissors, Rock) => 6 + u,  // win cases
        (Rock, Paper) => 6 + u,
        (Paper, Scissors) => 6 + u,
        _ => 3 + u      // tie
    }
}

fn main() {
    let stdin = io::stdin();

    let mut my_score = 0;
    for line in stdin.lines() {
        let s = line.expect("Error Reading Line");
        let mut chars : Chars = s.chars();

        let oc = chars.nth(0).unwrap();
        let uc = chars.nth(1).unwrap();
        let them = GameMove::from_char(oc);
        let us = GameMove::from_char(uc);

        my_score += score(them, us);
    }
    println!("my score: {}", my_score);
}
