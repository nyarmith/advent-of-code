use std::io;

fn main(){
    let mut user_input = String::new(); // input buffer
    let stdin = io::stdin();    // construct a handle to the std input of current process
    // each handle returned is a reference to a shared global buffer whose access is synchronized via a mutex
    let mut max_cal : u64 = 0;
    let mut tmp_buf : u64 = 0;
    while stdin.read_line(&mut user_input).unwrap() != 0 {
        match user_input.as_str() {
            "\n" => {
                if tmp_buf > max_cal { max_cal = tmp_buf; }
                tmp_buf = 0;
            }
            _ => {
                // https://users.rust-lang.org/t/trim-string-in-place/15809/7
                user_input.truncate(user_input.trim_end().len());
                tmp_buf += user_input.parse::<u64>().expect("Couldn't parse u64");
            }
        }
        user_input.clear()
    }

    if tmp_buf > max_cal { max_cal = tmp_buf; }

    println!("max cals: {}", max_cal);
}
