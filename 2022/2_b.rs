use std::io;
use std::str::Chars;
use GameMove::*;

#[derive(PartialEq, Eq, Copy, Clone)]
enum GameMove {
    Rock = 1,
    Paper = 2,
    Scissors = 3
}


trait FromChar {
    fn from_char(c: char) -> Self;
}

impl FromChar for GameMove {
    fn from_char(c: char) -> Self {
         match c {
            'A' => Rock,
            'B' => Paper,
            'C' => Scissors,
             _  => panic!("Unexpected character {} in from_char", c)
         }
     }
}

fn ganar(other: GameMove, us: char) -> u64 {
    match (other, us) {
        (Rock,      'Z') => 6 + Paper    as u64,      // win cases
        (Paper,     'Z') => 6 + Scissors as u64,
        (Scissors,  'Z') => 6 + Rock     as u64,
        (Rock,      'X') => 0 + Scissors as u64,      // lose cases
        (Paper,     'X') => 0 + Rock     as u64,
        (Scissors,  'X') => 0 + Paper    as u64,
        (_,         'Y') => 3 + other    as u64,      // tie cases
        _  => panic!("Unexpected char in ganar!")
    }
}

fn main() {
    let stdin = io::stdin();

    let mut my_score = 0;
    for line in stdin.lines() {
        let s = line.expect("Error Reading Line");
        let mut chars : Chars = s.chars();

        let oc = chars.nth(0).unwrap();
        let us = chars.nth(1).unwrap();
        let them = GameMove::from_char(oc);

        my_score += ganar(them, us);
    }
    println!("my score: {}", my_score);
}
